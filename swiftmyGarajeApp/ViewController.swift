//
//  ViewController.swift
//  swiftmyGarajeApp
//
//  Created by Victor Hugo Benitez Bosques on 11/09/16.
//  Copyright © 2016 Victor Hugo Benitez Bosques. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var labelBrand: UILabel!
    @IBOutlet var labelModel: UILabel!
    @IBOutlet var labelCV: UILabel!
    @IBOutlet var labelKms: UILabel!
    
    var myGaraje : [Car] = [] //array de objetos de tipo Car vacia
    var myCar : Car! //instancia de la clase Car
    var carID : Int! //contador que recorrera el array myGaraje
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //creacion de los varoles por defecto de la instanca myCar
        myCar = Car(cv: 140, marca: "Ford", modelo: "xxl", color: #colorLiteral(red: 0.2193539292, green: 0.4209204912, blue: 0.1073316187, alpha: 1), kms: 140000, image: #imageLiteral(resourceName: "ford"))
        myGaraje.append(myCar)
        
        myCar = Car(cv: 80, marca: "Passenger", modelo: "version 1.0.1", color: #colorLiteral(red: 1, green: 0.99997437, blue: 0.9999912977, alpha: 1), kms: 120000, image: #imageLiteral(resourceName: "passengerVW"))
        myGaraje.append(myCar)
        
        myCar = Car(cv: 100, marca: "vanVW", modelo: "MSND123", color: #colorLiteral(red: 0.1519963741, green: 0.2774518132, blue: 0.07954674214, alpha: 1), kms: 200000, image: #imageLiteral(resourceName: "vanVW"))
        myGaraje.append(myCar)
        
        myCar = Car(cv: 200, marca: "Ferrari", modelo: "XXKL", color: #colorLiteral(red: 0.8949507475, green: 0.1438436359, blue: 0.08480125666, alpha: 1), kms: 30000, image: #imageLiteral(resourceName: "ferrari"))
        myGaraje.append(myCar)
        
        myCar = Car(cv: 170, marca: "Bugatti", modelo: "XXS", color: #colorLiteral(red: 0.1431525946, green: 0.4145618975, blue: 0.7041897774, alpha: 1), kms: 20000, image: #imageLiteral(resourceName: "bugatti-1"))
        myGaraje.append(myCar)
        
        carID = 0 //posicion inicial del contador
        updateView() //actualizamos la vista despues de la instancia overwrite
    
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func updateView() { //asignacion de los atributos de la instancia myCar a la vista
        
        myCar = myGaraje[carID] //asignacion del objeto segun el incremento en carID
        UIView.animate(withDuration: 1.5, delay: 0.25, options: [.curveEaseIn, .curveLinear], animations: { //complexion hadler, animation se ejecutan de manera asincrona de forma paralelo
           
            if let myCarImage = self.myCar.image{
                self.imageView.image = myCarImage //binding si hay una imagen en la instancia
            }else{
                self.imageView.image = nil
            }
            self.labelBrand.text = "MARCA: \(self.myCar.marca!)"
            self.labelCV.text =  "CV: \(self.myCar.cv!) - W: \(self.myCar.powerToWhatts())"
            self.labelModel.text = "MODELO: \(self.myCar.modelo!)"
            self.labelKms.text = "KMS: \(self.myCar.kms!)"
            
            self.view.backgroundColor = self.myCar.color //propiedad cambia el color de la vista

        }){(completed) in       //Se ejecuta al terminar la animacion, se pueden encadenar las animaciones
            print("Animacion Completada!!! Recuerda que la maxima velocidad es de \(Car.maxSpeed())") //la propia clase manda a llamar las funciones que no depende del objeto
        }
    }
    
    @IBAction func buttonChangeCar(_ sender: UIButton) {
        carID = carID + 1 //cambiar de objeto coche incremento a 1
        
        if carID >= myGaraje.count{
            carID = 0
        }
        updateView() //mostramos de nuevo la vista con el index carID cambiado
    }
    
    @IBAction func buttonAcelerar(_ sender: UIButton) {
        self.myCar.acceletare()
    }
    
    @IBAction func addKms(_ sender: UIButton) {
        self.myCar.addKms(kmsToAdd: 10.0)
        updateView()
    }
    

}

