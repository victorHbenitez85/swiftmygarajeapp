//
//  Car.swift
//  swiftmyGarajeApp
//
//  Created by Victor Hugo Benitez Bosques on 11/09/16.
//  Copyright © 2016 Victor Hugo Benitez Bosques. All rights reserved.
//  car Class

import Foundation
import UIKit
class Car: NSObject { //toda clase hereda de NSObject
    
    //Set attributes
    var cv : Int!
    var marca : String!
    var modelo : String!
    var color : UIColor!    //Needs UIKit Librery
    var kms : Double!
    var image : UIImage?
    
    
    /*Computed properties: variable y funcion de NSObject que muestra la descripcion de una instancia sin definir el atributo a mostrar*/
    override var description: String{
        return "El Coche es un \(marca!) \(modelo!), tiene \(cv!) caballos de potencia y lleva hechos \(kms!) kms"
    }
    
    /*Default Constructor*/
    override init() {   //Initializes the attributes
        cv = 0
        marca = "Desconocida"
        modelo = "Desconocido"
        color = #colorLiteral(red: 0.7540004253, green: 0, blue: 0.2649998069, alpha: 1)
        kms = 0.0
    }
    
    init(cv: Int, marca: String, modelo: String, color: UIColor, kms: Double, image: UIImage?) { //Overwrite constructor
        self.cv = cv        //self = this
        self.marca = marca
        self.modelo = modelo
        self.color = color
        self.kms = kms
        
        if let image = image{ //asignacion de la imagen UIImage pasado por parametro
            self.image = image
        }
        
    }
    
    func addKms(kmsToAdd: Double) {
        self.kms = self.kms + kmsToAdd
    }
    
    func acceletare() {
        print("BBBBRRRRRRRRRRRR")
    }
    
    class func maxSpeed() -> Int { //funcion que no depende de las instancias
        return 120
    }
    
    func powerToWhatts() -> Int {
        return 750 * self.cv
    }
    
    

    
    
    
    
}
